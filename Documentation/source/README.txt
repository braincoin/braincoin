.. _README:  
  
=============
README
=============


BrainCoin is a version of Ubuntu designed to facilitate healthy digital communication. Based off of Ubuntu16.04, this distro has the stock applications one needs to lead a secure and social computing existence.


Installation
-----------------

Install BrainCoin either by downloading a RAW dump of the VMDK disk image:

[insert Vault link]


Chef recipe (Not yet implemented) 
~~~~~~~~~~~~~~~~~~~~~~

https://learn.chef.io/tutorials/learn-the-basics/

Debian meta-package (Not yet implemented)
~~~~~~~~~~~~~~~~~~~~~~

http://bredsaal.dk/a-quick-guide-to-debian-metapackages

https://www.gnu.org/licenses/gpl-howto.en.html


Post-install
-----------------

User Configuration
~~~~~~~~~~~~~~~~~~~~~~

Change your linux username/password, and all of the application usernames and passwords.

http://askubuntu.com/questions/34074/how-do-i-change-my-username

- sudo adduser temporary
- Change the account type of your new temporary user to administrator: System settings > users account. Select the temporary user, click Unlock then change account type to "administrator".
- log out
- log in as temporary
- sudo usermod -l newUsername oldUsername
- usermod -d /home/BrainCoinee -m newUsername
- log out
- log in as new user
- sudo deluser temporary
- sudo rm -r /home/temporary


Change disk size
~~~~~~~~~~~~~~~~~~~~~~


On Mac:

VBoxManage clonehd Ubuntu1604.vdi /path/to/BrainCoin.raw --format RAW

sudo dd if=/path/to/BrainCoin.raw of=/dev/disk4 bs=512


On Linux:

https://www.turnkeylinux.org/blog/convert-vm-iso

VBoxManage clonehd Ubuntu1604.vdi /path/to/BrainCoin.raw --format RAW
sudo dd if=/path/to/BrainCoin.raw of=/dev/diskX bs=512


http://www.ubergizmo.com/how-to/resize-virtualbox-disk/

VBoxManage modifyhd "/path/to/BrainCoinee.vdi" --resize 45000

45000 = 45GB


Next, extend your filesystem to cover the amount of diskspace you'd like.

https://www.turnkeylinux.org/blog/extending-lvm



Contribute
-----------------

- Source Code: bitbucket.org/$project/$project

Support
-----------------

If you are having issues, please let us know.

License
-------

The project is licensed under the GNU GPL 3 license.

